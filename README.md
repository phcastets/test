# SparseArray

SparseArray counts the number of time each query within a list of queries appears inside a predefined list of strings.


### Prerequisites 

* [Docker](https://docs.docker.com/docker-for-windows/install/) is  necessary to run SparseArray.

* The strings list can be found and modified in the Dockerfile if necessary. It is defined as an environment variable.

```bash
ENV STRINGS 'ab ab abc'
``` 
* The queries list is passed as an argument of the docker run command. For example:

```bash
docker run test ab,abc,bc
``` 

## Running the tests

Once Docker is installed, we can run SparseArray from the terminal:

**1. Change the working directory**

```bash
cd SparseArray
``` 

**2. Create the image from the dockerfile and tags the container with the name "test"**

```bash
docker build -t test .
```
**3. Create and start the container**

```bash
docker run test ab,abc,bc
```

The output is a dictionary returning the number of time each query within the list of queries appears inside the predefined list of strings. With the current parameters, we obtain:
**{ab: 2, abc: 1, bc: 0}**. 

The length of each list is also printed



## Author

**Paul-Henri Castets** 