# Set up Python 3.7 Alpine image
FROM python:alpine3.7

# Define the directory of the image filesystem
ENV INSTALL_PATH /usr/src/app  

# Define the directory of the image filesystem
ENV STRINGS 'ab ab abc'

# Create the directory in the container
RUN mkdir $INSTALL_PATH 

# Copy the files from host to image filesystem
COPY . .

# Give authorised access to the entry point file
RUN chmod 777 /entrypoint.sh

# Launch the Python script
ENTRYPOINT ["/entrypoint.sh"]

