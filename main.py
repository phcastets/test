from SparseArray import SparseArray

def main():
	# Instantiate the class SparseArray
	run = SparseArray()
	# Obtain the count of each query inside the list of strings
	run.matching_strings()
	# Obtain the size of both the queries list and the strings list
	run.sizes()

if __name__ == "__main__":
    main()