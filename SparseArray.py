import os
import sys

class SparseArray:
    '''Counts the number of time each query within a list of queries appears inside a predefined list of strings'''

    def __init__(self):
        # Make a list of strings from the environment variable STRING
        self.strings = os.environ.get('STRINGS').split(' ')
        # Group the arguments (queries) inside a list
        self.queries = sys.argv[1].split(',')

        
    def matching_strings(self):
        # Initialize the count of each query at 0
        self.results = {q:0 for q in self.queries}
        # Add 1 to the count each time the query is found among the strings
        for q in self.queries:
            for s in self.strings:
                if q==s:
                    self.results[q]+=1
        print(self.results)

    
    def sizes(self):
        # Compute the size of each list
        self.queries_size = len(self.queries)
        self.strings_size = len(self.strings)
        
        # Print the size of each list
        size_message = print('The length of the queries list is %d \nThe length of the strings list is %d' 
                             %(self.queries_size, self.strings_size))
        return size_message 